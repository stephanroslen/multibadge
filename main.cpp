// 2019 by Stephan Roslen

#include <iostream>
#include <type_traits>

template <typename T>
struct SingleBadge {
 private:
  constexpr SingleBadge() {};
  friend T;
};

template <typename ...Ts>
struct MultiBadge {
 public:
  template <
      typename T,
      typename SFINAE = std::enable_if_t<
          std::disjunction_v<std::is_same<T, Ts>...>>>
  constexpr MultiBadge(SingleBadge<T>) {};
 private:
  MultiBadge() = delete;
};

/**
 * Didn't find a more beautiful approach to help classes deduce their own
 * SingleBadge. But this will do.
 */
#define DECLARE_BADGE()                                  \
constexpr auto Badge(void) {                             \
  return SingleBadge<std::decay_t<decltype(*this)>>{};   \
}

class A;
class B;
class C;
class D;

struct MultiBadgeTest {
  int TestFun(MultiBadge<A, B, C>, int par) {
    return par + 3;
  }
};

class A {
  DECLARE_BADGE()

 public:
  int TestFun(int par) {
    return MultiBadgeTest{}.TestFun(Badge(), par);
  }
};

class B {
  DECLARE_BADGE()

 public:
  int TestFun(int par) {
    return MultiBadgeTest{}.TestFun(Badge(), par);
  }
};

class C {
  DECLARE_BADGE()

 public:
  int TestFun(int par) {
    return MultiBadgeTest{}.TestFun(Badge(), par);
  }
};

#ifdef DOFAIL
class D {
  DECLARE_BADGE()

 public:
  int TestFun(int par) {
    return MultiBadgeTest{}.TestFun(Badge(), 3);
  }
};
#endif

int main(int argc, const char *argv[]) {
  std::cout << A{}.TestFun(-3) << "\n";
  std::cout << B{}.TestFun(-2) << "\n";
  std::cout << C{}.TestFun(-1) << "\n";
#ifdef DOFAIL
  std::cout << D{}.TestFun(0) << "\n";
#endif
  return 0;
}